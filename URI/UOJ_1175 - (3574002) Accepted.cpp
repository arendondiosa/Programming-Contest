#include <bits/stdc++.h>
#define fast ios_base::sync_with_stdio(false);cin.tie(NULL)

using namespace std;

int main() {
  fast;
  vector<int> vec(20);

  for (int i = 19; i >= 0; i--) cin >> vec [i];
  for (int i = 0; i < 20; i++) cout << "N[" << i << "] = " << vec[i] << endl;

  return 0;
}
