#include <bits/stdc++.h>
#define fast ios_base::sync_with_stdio(false);cin.tie(NULL)

using namespace std;

int main(){
  fast;
  int n, k;
  cin >> n;
  for (int i = 1; i <= n; i++) {
    cin >> k;
    vector<int> vec(k);
    for (int j = 0; j < k; j++) cin >> vec[j];
    sort(vec.begin(), vec.end());
    cout << "Case " << i << ": " << vec[k / 2] << endl;
  }

	return 0;
}
