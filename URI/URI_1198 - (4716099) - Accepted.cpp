#include <bits/stdc++.h>
#define fast ios_base::sync_with_stdio(false); cin.tie(NULL);
#define endl '\n'

using namespace std;

int main() {
  fast;
  long long x, y;

  while (cin >> x >> y) {
    cout << abs(x - y) << endl;
  }

  return 0;
}
