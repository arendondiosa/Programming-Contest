#include <bits/stdc++.h>
#define fast ios_base::sync_with_stdio(false);cin.tie(NULL)

using namespace std;

int main() {
  fast;
  int a, n;
  unsigned long long sol = 0;
  cin >> a;
  while (cin >> n) {
    if (n > 0) {
      for (int i = a; i < a + n; i++) sol += i;
      break;
    }
  }
  cout << sol << endl;

  return 0;
}
