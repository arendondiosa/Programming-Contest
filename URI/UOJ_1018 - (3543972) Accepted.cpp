#include <bits/stdc++.h>
#define fast ios_base::sync_with_stdio(false);cin.tie(NULL)

using namespace std;

int main() {
  fast;
  int n;
  int vec[] = {100, 50, 20, 10, 5, 2, 1};
  vector<int> result (7);
  cin >> n;
  cout << n << endl;

  result[0] = n / vec[0]; n = n % vec[0];
  result[1] = n / vec[1]; n = n % vec[1];
  result[2] = n / vec[2]; n = n % vec[2];
  result[3] = n / vec[3]; n = n % vec[3];
  result[4] = n / vec[4]; n = n % vec[4];
  result[5] = n / vec[5]; n = n % vec[5];
  result[6] = n / vec[6]; n = n % vec[6];

  for (int i = 0; i < 7; i++) {
    cout << result[i] << " nota(s) de R$ " << vec[i] << ",00" << endl;
  }

  return 0;
}
