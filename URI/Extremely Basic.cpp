//Extremely Basic
//https://www.urionlinejudge.com.br/judge/es/problems/view/1001

#include <bits/stdc++.h>
#define fast ios_base::sync_with_stdio(false);cin.tie(NULL)

using namespace std;

int main() {
  fast;
  int a, b;
  cin >> a >> b;
  cout << "X = " << a + b << endl;

  return 0;
}
