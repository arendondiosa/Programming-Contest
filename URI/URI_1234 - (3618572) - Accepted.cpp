#include <bits/stdc++.h>
#define fast ios_base::sync_with_stdio(false);cin.tie(NULL)

using namespace std;

int main() {
  fast;
  string x;
  int count;

  while (getline(cin, x)) {
    count = 0;
    for (int i = 0; i < x.size(); i++) {
      if (x[i] == ' ') cout << " ";
      else {
        if (count % 2 == 0) {
          if (x[i] >= 'a' && x[i] <= 'z') cout << (char)(x[i] - 32);
          else cout << x[i];
          count++;
        }
        else {
          if (x[i] >= 'A' && x[i] <= 'Z') cout << (char)(x[i] + 32);
          else cout << x[i];
          count++;
        }
      }
      if (i == x.size() - 1) cout << endl;
    }
  }

  return 0;
}
