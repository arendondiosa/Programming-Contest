import java.io.*;
import java.util.*;

public class Main {
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        StringTokenizer st;
        
        String x, a, b;
        int n;
        
        n = Integer.valueOf(br.readLine());
        for (int i = 0; i < n; i++) {
            x = br.readLine();
            a = x.substring(0, x.length()/2);
            b = x.substring(x.length()/2, x.length());
            System.out.println(new StringBuilder(a).reverse().toString() + new StringBuilder(b).reverse().toString());
        }
    }
}
