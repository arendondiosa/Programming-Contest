#include <bits/stdc++.h>
#define fast ios_base::sync_with_stdio(false);cin.tie(NULL)
#define pi 3.14159

using namespace std;

int main() {
  fast;
  double x;
  scanf("%lf", &x);
  double out = (4.0/3.0) * pi * (x * x * x);
  printf("VOLUME = %.3lf\n", out);

  return 0;
}
