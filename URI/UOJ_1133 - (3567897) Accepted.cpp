﻿<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />        <title>
            URI Online Judge - Errors        </title>

        <meta name="description" content="URI Online Judge - Errors" />
        <meta name="keywords" content="URI Online Judge, urionlinejudge, uri, online judge, judge, problems, beginner" />
        <meta name="revisit-after" content="1 days" />
        <meta name="robots" content="index, nofollow" />

        <link href="https://urionlinejudge.r.worldssl.net/judge/img/favicon.ico" type="image/x-icon" rel="icon" /><link href="https://urionlinejudge.r.worldssl.net/judge/img/favicon.ico" type="image/x-icon" rel="shortcut icon" />
	<link rel="stylesheet" type="text/css" href="https://urionlinejudge.r.worldssl.net/judge/css/error.css" />
    </head>
    <body>

        
<div>            
    <div id="logo"></div>

    <div class="description">
        <h1>Não Encontrado</h1>
        A URL solicitada não foi encontrada neste servidor.<br/>
        <a href="https://www.urionlinejudge.com.br">Clique aqui para voltar ao portal.</a>
    </div>

    <div class="description">
        <h1>Not Found</h1>
        The requested URL was not found on this server.<br/>
        <a href="https://www.urionlinejudge.com.br">Click here to return to the website.</a>
    </div>            
</div>
    </body>
</html>
﻿<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />        <title>
            URI Online Judge - Errors        </title>

        <meta name="description" content="URI Online Judge - Errors" />
        <meta name="keywords" content="URI Online Judge, urionlinejudge, uri, online judge, judge, problems, beginner" />
        <meta name="revisit-after" content="1 days" />
        <meta name="robots" content="index, nofollow" />

        <link href="https://urionlinejudge.r.worldssl.net/judge/img/favicon.ico" type="image/x-icon" rel="icon" /><link href="https://urionlinejudge.r.worldssl.net/judge/img/favicon.ico" type="image/x-icon" rel="shortcut icon" />
	<link rel="stylesheet" type="text/css" href="https://urionlinejudge.r.worldssl.net/judge/css/error.css" />
    </head>
    <body>

        
<div>            
    <div id="logo"></div>

    <div class="description">
        <h1>Não Encontrado</h1>
        A URL solicitada não foi encontrada neste servidor.<br/>
        <a href="https://www.urionlinejudge.com.br">Clique aqui para voltar ao portal.</a>
    </div>

    <div class="description">
        <h1>Not Found</h1>
        The requested URL was not found on this server.<br/>
        <a href="https://www.urionlinejudge.com.br">Click here to return to the website.</a>
    </div>            
</div>
    </body>
</html>
﻿<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />        <title>
            URI Online Judge - Errors        </title>

        <meta name="description" content="URI Online Judge - Errors" />
        <meta name="keywords" content="URI Online Judge, urionlinejudge, uri, online judge, judge, problems, beginner" />
        <meta name="revisit-after" content="1 days" />
        <meta name="robots" content="index, nofollow" />

        <link href="https://urionlinejudge.r.worldssl.net/judge/img/favicon.ico" type="image/x-icon" rel="icon" /><link href="https://urionlinejudge.r.worldssl.net/judge/img/favicon.ico" type="image/x-icon" rel="shortcut icon" />
	<link rel="stylesheet" type="text/css" href="https://urionlinejudge.r.worldssl.net/judge/css/error.css" />
    </head>
    <body>

        
<div>            
    <div id="logo"></div>

    <div class="description">
        <h1>Não Encontrado</h1>
        A URL solicitada não foi encontrada neste servidor.<br/>
        <a href="https://www.urionlinejudge.com.br">Clique aqui para voltar ao portal.</a>
    </div>

    <div class="description">
        <h1>Not Found</h1>
        The requested URL was not found on this server.<br/>
        <a href="https://www.urionlinejudge.com.br">Click here to return to the website.</a>
    </div>            
</div>
    </body>
</html>
﻿<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />        <title>
            URI Online Judge - Errors        </title>

        <meta name="description" content="URI Online Judge - Errors" />
        <meta name="keywords" content="URI Online Judge, urionlinejudge, uri, online judge, judge, problems, beginner" />
        <meta name="revisit-after" content="1 days" />
        <meta name="robots" content="index, nofollow" />

        <link href="https://urionlinejudge.r.worldssl.net/judge/img/favicon.ico" type="image/x-icon" rel="icon" /><link href="https://urionlinejudge.r.worldssl.net/judge/img/favicon.ico" type="image/x-icon" rel="shortcut icon" />
	<link rel="stylesheet" type="text/css" href="https://urionlinejudge.r.worldssl.net/judge/css/error.css" />
    </head>
    <body>

        
<div>            
    <div id="logo"></div>

    <div class="description">
        <h1>Não Encontrado</h1>
        A URL solicitada não foi encontrada neste servidor.<br/>
        <a href="https://www.urionlinejudge.com.br">Clique aqui para voltar ao portal.</a>
    </div>

    <div class="description">
        <h1>Not Found</h1>
        The requested URL was not found on this server.<br/>
        <a href="https://www.urionlinejudge.com.br">Click here to return to the website.</a>
    </div>            
</div>
    </body>
</html>
#include <bits/stdc++.h>
#define fast ios_base::sync_with_stdio(false);cin.tie(NULL)

using namespace std;

int main() {
  fast;
  int x, y;
  cin >> x >> y;

  for (int i = min(x, y) + 1; i < max(x, y); i++) {
    int aux = i % 5;
    if (aux == 2 || aux == 3) cout << i << endl;
  }

  return 0;
}
