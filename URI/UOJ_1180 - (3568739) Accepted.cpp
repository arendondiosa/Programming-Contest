#include <bits/stdc++.h>
#define fast ios_base::sync_with_stdio(false);cin.tie(NULL)

using namespace std;

int main() {
  fast;
  int n, aux, index, mini = INT_MAX;
  cin >> n;

  for (int i = 0; i < n; i++) {
    cin >> aux;
    if (aux < mini) {
      mini = aux;
      index = i;
    }
  }
  cout << "Menor valor: " << mini << endl;
  cout << "Posicao: " << index << endl;

  return 0;
}
