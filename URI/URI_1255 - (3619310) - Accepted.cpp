#include <bits/stdc++.h>
#define fast ios_base::sync_with_stdio(false);cin.tie(NULL)

using namespace std;

int toInt(string x) {
  stringstream s; s << x; int r; s >> r; return r;
}

int main() {
  fast;
  int n;
  string x, y;
  getline(cin, y);
  n = toInt(y);
  for (int i = 0; i < n; i++) {
    vector<int> vec(27);
    getline(cin, x);
    for (int j = 0; j < x.size(); j++) {
      if (x[j] >= 'A' && x[j] <= 'Z') vec[tolower(x[j]) - 'a']++;
      else if (x[j] >= 'a' && x[j] <= 'z') vec[x[j] - 'a']++;
    }

    vector<int> vaux = vec;
    sort(vec.rbegin(), vec.rend());
    int maxi = vec[0];

    for (int j = 0; j < vec.size(); j++) {
      if (vaux[j] == maxi) cout << (char)(j + 'a');
    }
    cout << endl;
  }

  return 0;
}
