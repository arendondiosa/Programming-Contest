#include <bits/stdc++.h>
#define fast ios_base::sync_with_stdio(false);cin.tie(NULL)

using namespace std;

int main() {
  fast;
  int n;
  vector<int> vec(47);
  vec[0] = 0; vec[1] = 1;
  for (int i = 2; i < 47; i++) vec[i] = vec[i - 1] + vec[i - 2];

  cin >> n;
  for (int i = 0; i < n; i++) {
    cout << vec[i];
    if (i == n - 1) cout << endl;
    else cout << " ";
  }

  return 0;
}
