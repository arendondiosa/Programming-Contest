#include <bits/stdc++.h>
#define fast ios_base::sync_with_stdio(false);cin.tie(NULL)

using namespace std;

int main() {
  fast;
  int x;
  long long sol;

  while (cin >> x and x != 0) {
    sol = 0;
    int ini;
    if (x % 2 == 0) ini = x;
    else ini = x + 1;

    for (int i = ini; i < ini + (10); i += 2) sol += i;
    cout << sol << endl;
  }

  return 0;
}
