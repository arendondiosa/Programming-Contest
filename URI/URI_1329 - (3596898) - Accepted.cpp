#include <bits/stdc++.h>
#define fast ios_base::sync_with_stdio(false);cin.tie(NULL)

using namespace std;

int main(){
  fast;
  int n, x;
  while (cin >> n and n > 0) {
    int j = 0, m = 0;
    for (int i = 0; i < n; i++) {
      cin >> x;
      if (x == 1) j++;
      else m++;
    }
    cout << "Mary won " << m << " times and John won " << j << " times" << endl;
  }

	return 0;
}
