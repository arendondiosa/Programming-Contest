#include <bits/stdc++.h>

using namespace std;

int main() {
  int n;
  string a, b, aux;
  bool flag;
  cin >> n;

  for (int i = 0; i < n; i++) {
    flag = false;
    cin >> a >> b;
    if (a.size() >= b.size()) {
      aux = a.substr((a.size() - 1) - (b.size() - 1));
      //cout << aux << endl;
      if (b.compare(aux) == 0) flag = true;
    }

    if (flag) cout << "encaixa" << endl;
    else cout << "nao encaixa" << endl;
  }

  return 0;
}
