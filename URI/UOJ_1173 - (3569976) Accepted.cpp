#include <bits/stdc++.h>
#define fast ios_base::sync_with_stdio(false);cin.tie(NULL)

using namespace std;

int main() {
  fast;
  int n;
  cin >> n;
  vector<int> vec(10);
  vec[0] = n;
  cout << "N[0] = " << vec[0] << endl;
  for (int i = 1; i < 10; i++) {
    vec[i] = vec[i - 1] * 2;
    cout << "N[" << i << "] = " << vec[i] << endl;
  }

  return 0;
}
