#include <bits/stdc++.h>
#define fast ios_base::sync_with_stdio(false);cin.tie(NULL)

using namespace std;

int main() {
  fast;
  int t, x, y;
  cin >> t;
  for (int i = 0; i < t; i++) {
    cin >> x >> y;
    int ini, sol = 0;
    if (x % 2 == 0) ini = x + 1;
    else ini = x;

    for (int j = ini; j < ini + (y * 2); j += 2) sol += j;
    cout << sol << endl;
  }

  return 0;
}
