import java.io.*;
import java.util.*;

public class Main {
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        Scanner sc = new Scanner(System.in);
        
        int n;
        String[] vec;
        int cont = 0;
        while(true) {
            n = Integer.valueOf(br.readLine());
            if (n == 0) break;
            else {
                if (cont > 0) System.out.println("");
                cont += 1;
                int maxi = 0;
                vec = new String[n];
                for(int i = 0; i < n ; i++) {
                    vec[i] = br.readLine();
                    if (vec[i].length() > maxi) maxi = vec[i].length();
                }
                for (int i = 0; i < n; i++) {
                    if(vec[i].length() < maxi) {
                        for (int j = 0; j < maxi - vec[i].length(); j++) System.out.print(" ");
                    }
                    System.out.println(vec[i]);
                }
            }
        }     
    }
}
