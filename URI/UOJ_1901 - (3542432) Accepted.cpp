#include <bits/stdc++.h>
#define fast ios_base::sync_with_stdio(false);cin.tie(NULL)

using namespace std;

int main() {
  fast;
  int n, x, y;
  cin >> n;
  int mat[n][n];
  set<int> res;

  for (int i = 0; i < n; i++)
    for (int j = 0; j < n; j++)
      cin >> mat[i][j];

  for (int i = 0; i < (n << 1); i++) {
    cin >> x >> y;
    res.insert(mat[x - 1][y - 1]);
  }

  cout << res.size() << endl;
  return 0;
}
