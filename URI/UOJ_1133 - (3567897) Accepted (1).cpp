#include <bits/stdc++.h>
#define fast ios_base::sync_with_stdio(false);cin.tie(NULL)

using namespace std;

int main() {
  fast;
  int x, y;
  cin >> x >> y;

  for (int i = min(x, y) + 1; i < max(x, y); i++) {
    int aux = i % 5;
    if (aux == 2 || aux == 3) cout << i << endl;
  }

  return 0;
}
