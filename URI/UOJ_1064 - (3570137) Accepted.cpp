#include <bits/stdc++.h>
#define fast ios_base::sync_with_stdio(false);cin.tie(NULL)

using namespace std;

int main() {
  fast;
  double n, out = 0.0;
  int count = 0;
  for (int i = 0; i < 6; i++) {
    cin >> n;
    if (n > 0.0){
      out += n;
      count++;
    }
  }
  cout << count << " valores positivos" << endl;
  cout << out/count << endl;

  return 0;
}
