#include <bits/stdc++.h>
#define fast ios_base::sync_with_stdio(false);cin.tie(NULL)

using namespace std;

int main() {
  fast;
  int a, b, beg, end;
  long long out = 0;
  cin >> a >> b;
  if (a > b) {
    beg = b;
    end = a;
  }
  else {
    beg = a;
    end = b;
  }

  for (int i = beg + 1; i < end; i++) {
    if (i % 2 != 0) out += i;
  }

  cout << out << endl;

  return 0;
}
