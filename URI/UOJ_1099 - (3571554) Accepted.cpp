#include <bits/stdc++.h>
#define fast ios_base::sync_with_stdio(false);cin.tie(NULL)

using namespace std;

int main() {
  fast;
  int t, x, y;
  cin >> t;
  for (int i = 0; i < t; i++) {
    int out = 0;
    cin >> x >> y;
    if (x != y) {
      for (int i = min(x, y) + 1; i < max(x, y); i++) {
        if (i % 2 != 0) out += i;
      }
    }
    cout << out << endl;
  }
}
