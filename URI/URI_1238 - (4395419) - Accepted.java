import java.io.*;
import java.util.*;

public class Main {
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        StringTokenizer st;
        String x, a, b;
        int n;
        
        n = Integer.valueOf(br.readLine());
        for (int i = 0; i < n; i++) {
            x = br.readLine();
            st = new StringTokenizer(x);
            a = st.nextToken();
            b = st.nextToken();
            
            for(int j = 0; j < Math.max(a.length(), b.length()); j++) {
                if(j < a.length()) System.out.print(a.charAt(j));
                if(j < b.length()) System.out.print(b.charAt(j));
            }
            System.out.println("");
        }
    }
}
