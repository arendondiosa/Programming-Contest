#include <bits/stdc++.h>
#define fast ios_base::sync_with_stdio(false);cin.tie(NULL)

using namespace std;

int main() {
  fast;
  float x, y;
  scanf("%f %f", &x, &y);
  printf("%.3f km/l\n", x / y);

  return 0;
}
