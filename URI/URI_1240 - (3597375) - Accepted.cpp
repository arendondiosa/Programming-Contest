#include <bits/stdc++.h>
#define fast ios_base::sync_with_stdio(false);cin.tie(NULL)

using namespace std;

int toInt(string s) {
  stringstream ss;
  ss << s;
  int n;
  ss >> n;
  return n;
}

int main() {
  fast;
  int t;
  string x, y;
  cin >> t;
  for (int i = 0; i < t; i++) {
    cin >> x >> y;
    bool flag = true;
    if (y.size() <= x.size()) {
      string aux = x.substr(x.size() - y.size());
      if (toInt(y) == toInt(aux)) flag = true;
      else flag = false;
    }
    else flag = false;

    if (flag) cout << "encaixa" << endl;
    else cout << "nao encaixa" << endl;
  }

  return 0;
}
