#include <bits/stdc++.h>
#define fast ios_base::sync_with_stdio(false); cin.tie(NULL);
#define endl '\n'

using namespace std;

int main() {
  fast;
  int n, count;
  double x;
  cin >> n;

  for (int i = 0; i < n; i++) {
    count = 0;
    cin >> x;

    while (x > 1.0) {
      x /= 2.0;
      count++;
    }

    cout << count << " dias" << endl;
  }

  return 0;
}
