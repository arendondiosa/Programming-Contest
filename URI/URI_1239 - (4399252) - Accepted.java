import java.io.*;
import java.util.*;

public class Main {
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        Scanner sc = new Scanner(System.in);
        String x;
        boolean flaga = false, flagb = false;
        
        while (sc.hasNext()) {
            x = sc.nextLine();
            for (int i = 0; i < x.length(); i++) {
                if (x.charAt(i) == '*') {
                    if (!flaga) {
                        System.out.print("<b>");
                        flaga = true;
                    }
                    else {
                        System.out.print("</b>");
                        flaga = false;
                    }
                }
                else if (x.charAt(i) == '_') {
                    if (!flagb) {
                        System.out.print("<i>");
                        flagb = true;
                    }
                    else {
                        System.out.print("</i>");
                        flagb = false;
                    }
                }
                else System.out.print(x.charAt(i));
            }
            System.out.println("");
        }
    }
}
