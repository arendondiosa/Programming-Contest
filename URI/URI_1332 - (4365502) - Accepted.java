import java.io.*;

public class Main {
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int n;
        String a;
        
        n = Integer.valueOf(br.readLine());
        
        for (int i = 0; i < n; i++) {
            a = br.readLine();
            if (a.length() == 3) {
                if((a.charAt(0) == 'o' && a.charAt(1) == 'n') || (a.charAt(1) == 'n' && a.charAt(2) == 'e') || (a.charAt(0) == 'o' && a.charAt(2) == 'e')) System.out.println("1");
                else System.out.println("2");
            }
            else if (a.length() == 5) System.out.println("3");
        }
        
    }
}
