#include <bits/stdc++.h>

using namespace std;

int main() {
  int n, a, c = 0, r = 0, s = 0, t = 0;
  char l;
  cin >> n;
  for (int i = 0; i < n; i++) {
    cin >> a >> l;
    t += a;
    if (l == 'C') c += a;
    else if (l == 'R') r += a;
    else if (l == 'S') s += a;
  }

  cout << "Total: " << t << " cobaias" << endl;
  cout << "Total de coelhos: " << c << endl;
  cout << "Total de ratos: " << r << endl;
  cout << "Total de sapos: " << s << endl;
  cout << "Percentual de coelhos: " << fixed << setprecision(2) << (double)((c * 100) / (double)t) << " %" << endl;
  cout << "Percentual de ratos: " << fixed << setprecision(2) << (double)((r * 100) / (double)t) << " %" << endl;
  cout << "Percentual de sapos: " << fixed << setprecision(2) << (double)((s * 100) / (double)t) << " %" << endl;

  return 0;
}
