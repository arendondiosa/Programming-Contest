#include <bits/stdc++.h>
#define fast ios_base::sync_with_stdio(false);cin.tie(NULL)

using namespace std;

bool valid(string x) {
  string y = "2002";
  if (x.size() != y.size()) return false;
  for (int i = 0; i < y.size(); i++) {
    if (y[i] != x[i]) return false;
  }

  return true;
}

int main() {
  fast;
  string x;
  while (cin >> x) {
    if (valid(x)) {
      cout << "Acesso Permitido" << endl;
      break;
    }
    else cout << "Senha Invalida" << endl;
  }

  return 0;
}
