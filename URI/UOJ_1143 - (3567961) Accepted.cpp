#include <bits/stdc++.h>
#define fast ios_base::sync_with_stdio(false);cin.tie(NULL)

using namespace std;

int main() {
  fast;
  int x;
  cin >> x;

  for (int i = 1; i <= x; i++) cout << i << " " << i * i << " " << i * i * i << endl;

  return 0;
}
