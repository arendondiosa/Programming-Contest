#include <bits/stdc++.h>
#define fast ios_base::sync_with_stdio(false);cin.tie(NULL)

using namespace std;

int main() {
  fast;
  int x, count = 0;
  for (int i = 0; i < 5; i++) {
    cin >> x;
    if (x % 2 == 0) count ++;
  }
  cout << count << " valores pares" << endl;

  return 0;
}
