#include <bits/stdc++.h>
#define fast ios_base::sync_with_stdio(false);cin.tie(NULL)

using namespace std;

int main() {
  fast;
  int x, beg;
  cin >> x;
  if (x % 2 == 0) beg = x + 1;
  else beg = x;

  for (int i = 0; i < 6; i++) {
    cout << beg << endl;
    beg += 2;
  }

  return 0;
}
