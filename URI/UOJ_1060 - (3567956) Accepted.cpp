#include <bits/stdc++.h>
#define fast ios_base::sync_with_stdio(false);cin.tie(NULL)

using namespace std;

int main() {
  fast;
  double x, count = 0;

  for (int i = 0; i < 6; i++) {
    cin >> x;
    if (x > 0.0) count ++;
  }
  cout << count << " valores positivos" << endl;

  return 0;
}
