#include <bits/stdc++.h>
#define fast ios_base::sync_with_stdio(false);cin.tie(NULL)

using namespace std;

int main() {
  fast;
  vector<int> vec(10);
  for (int i = 0; i < 10; i++) {
    cin >> vec[i];
    if (vec[i] <= 0) vec[i] = 1;
    cout << "X[" << i <<"] = " << vec[i] << endl;
  }

  return 0;
}
