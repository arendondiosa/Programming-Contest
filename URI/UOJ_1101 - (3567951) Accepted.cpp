#include <bits/stdc++.h>
#define fast ios_base::sync_with_stdio(false);cin.tie(NULL)

using namespace std;

int main() {
  fast;
  int m, n;

  while (cin >> m >> n and m > 0 and n > 0) {
    int count = 0;
    for (int i = min(m, n); i <= max(m, n); i++) {
      cout << i << " ";
      count += i;
    }
    cout << "Sum=" << count << endl;
  }

  return 0;
}
