#include <bits/stdc++.h>
#define fast ios_base::sync_with_stdio(false);cin.tie(NULL)

using namespace std;

int main() {
  fast;
  string vec[] = {"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"};
  int x;
  cin >> x;
  cout << vec[x - 1] << endl;    
  return 0;
}
