#include <bits/stdc++.h>
#define fast ios_base::sync_with_stdio(false);cin.tie(NULL)

using namespace std;

int main() {
  fast;
  vector<long long> vec(61);
  vec[0] = 0; vec[1] = 1;
  int t, n;
  for (int i = 2; i < vec.size(); i++) vec[i] = vec[i - 1] + vec[i - 2];
  cin >> t;
  for (int i = 0; i < t; i++) {
    cin >> n;
    cout << "Fib(" << n << ") = " << vec[n] << endl;
  }

  return 0;
}
