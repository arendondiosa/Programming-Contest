#include <bits/stdc++.h>
#define fast ios_base::sync_with_stdio(false);cin.tie(NULL)

using namespace std;

bool valid(double x) {
  if (x >= 0.0 && x <= 10.0) return true;
  return false;
}

int main() {
  fast;
  int i = 0;
  double x, sol = 0.0;

  while (cin >> x) {
    if (valid(x)) {
      sol += x;
      i++;
      if (i == 2) break;
    }
    else cout << "nota invalida" << endl;
  }
  cout << "media = " << fixed << setprecision(2) << sol/2.0 << endl;

  return 0;
}
