#include <bits/stdc++.h>
#define fast ios_base::sync_with_stdio(false);cin.tie(NULL)

using namespace std;

int main() {
  fast;
  double vec[] = {4.00, 4.50, 5.00, 2.00, 1.50};

  int x, y;
  scanf("%d %d", &x, &y);
  printf("Total: R$ %.2lf\n", vec[x - 1] * y);

  return 0;
}
