#include <bits/stdc++.h>
#define fast ios_base::sync_with_stdio(false);cin.tie(NULL)

using namespace std;

int main(){
  fast;
  vector<unsigned long long> fac(21);
  fac[0] = 1;
  for (int i = 1; i <= 20; i++) fac[i] = i * fac[i - 1];
  int m, n;
  while (cin >> m >> n) {
    unsigned long long sol = fac[m] + fac[n];
    cout << sol << endl;
  }

	return 0;
}
