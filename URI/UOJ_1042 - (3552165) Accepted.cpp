#include <bits/stdc++.h>
#define fast ios_base::sync_with_stdio(false);cin.tie(NULL)

using namespace std;

int main() {
  fast;
  vector<int> vec(3);
  vector<int> aux;
  cin >> vec[0] >> vec[1] >> vec[2];
  aux = vec;
  sort(vec.begin(), vec.end());

  for (int i = 0; i < 3; i++) cout << vec[i] << endl;
  cout << endl;

  for (int i = 0; i < 3; i++) cout << aux[i] << endl;

  return 0;
}
