#include <bits/stdc++.h>
#define fast ios_base::sync_with_stdio(false);cin.tie(NULL)

using namespace std;

int main() {
  fast;
  int x, y;
  long long out = 0;
  cin >> x >> y;

  for (int i = min(x, y); i <= max(x, y); i++) {
    if (i % 13 != 0) out += i;
  }
  cout << out << endl;

  return 0;
}
