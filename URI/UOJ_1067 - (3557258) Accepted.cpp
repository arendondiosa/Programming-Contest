#include <bits/stdc++.h>
#define fast ios_base::sync_with_stdio(false);cin.tie(NULL)

using namespace std;

int main() {
  fast;
  int x, end;
  cin >> x;
  if (x % 2 == 0) end = x - 1;
  else end = x;

  for (int i = 1; i <= end; i++) {
    cout << i << endl;
    i++;
  }

  return 0;
}
