#include <bits/stdc++.h>

using namespace std;

int main() {
  int n;
  cin >> n;

  for (unsigned long long i = 2; i <= n; i++) {
    unsigned long long sol = i * i;
    if (i % 2 == 0) cout << i << "^2 = " << sol << endl;
   }
  return 0;
}
