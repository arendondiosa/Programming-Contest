#include <bits/stdc++.h>
#define fast ios_base::sync_with_stdio(false);cin.tie(NULL)

using namespace std;

int main() {
  fast;
  int x, pos = 0, neg = 0, p = 0, im = 0;
  for (int i = 0; i < 5; i++) {
    cin >> x;
    if (x > 0) pos++;
    else if (x < 0) neg++;

    if (x % 2 == 0) p++;
    else im++;
  }

  cout << p << " valor(es) par(es)" << endl;
  cout << im << " valor(es) impar(es)" << endl;
  cout << pos << " valor(es) positivo(s)" << endl;
  cout << neg << " valor(es) negativo(s)" << endl;

  return 0;
}
