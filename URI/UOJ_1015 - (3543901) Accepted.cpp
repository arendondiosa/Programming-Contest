#include <bits/stdc++.h>
#define fast ios_base::sync_with_stdio(false);cin.tie(NULL)

using namespace std;

int main() {
  fast;
  float x1, y1, x2, y2;
  scanf("%f %f %f %f", &x1, &y1, &x2, &y2);
  printf("%.4f\n", sqrt(((x2 - x1) * (x2 - x1)) + ((y2 - y1) * (y2 - y1))));

  return 0;
}
