#include <bits/stdc++.h>
#define fast ios_base::sync_with_stdio(false);cin.tie(NULL)

using namespace std;

int main() {
  fast;
  int x;

  while (cin >> x and x > 0) {
    for (int i = 1; i <= x; i++) {
      if (i == x) cout << i << endl;
      else cout << i << " ";
    }
  }

  return 0;
}
