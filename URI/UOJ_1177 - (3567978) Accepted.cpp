#include <bits/stdc++.h>
#define fast ios_base::sync_with_stdio(false);cin.tie(NULL)

using namespace std;

int main() {
  fast;
  int t, aux = 0;;
  vector<int> vec(1000);
  cin >> t;

  for (int i = 0; i < 1000; i++) {
    vec[i] = aux;
    if (aux == t - 1) aux = 0;
    else aux++;

    cout << "N[" << i << "] = " << vec[i] << endl;
  }

  return 0;
}
