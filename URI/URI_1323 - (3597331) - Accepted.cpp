#include <bits/stdc++.h>
#define fast ios_base::sync_with_stdio(false);cin.tie(NULL)

using namespace std;

int main() {
  fast;
  vector<unsigned long long> vec(101);
  vec[0] = 0; vec[1] = 1;
  for (int i = 2; i <= 100; i++) vec[i] = vec[i - 1] + pow(i, 2);

  int x;
  while (cin >> x and x > 0) cout << vec[x] << endl;

  return 0;
}
