#include <bits/stdc++.h>
#define fast ios_base::sync_with_stdio(false);cin.tie(NULL)

using namespace std;

int main() {
  fast;
  int x, out = 0, index;

  for (int i = 0; i < 100; i++) {
    cin >> x;
    if (x > out) {
      out = x;
      index = i + 1;
    }
  }
  cout << out << endl;
  cout << index << endl;

  return 0;
}
