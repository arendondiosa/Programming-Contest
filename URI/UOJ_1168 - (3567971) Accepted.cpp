#include <bits/stdc++.h>
#define fast ios_base::sync_with_stdio(false);cin.tie(NULL)

using namespace std;

int main() {
  fast;
  int vec[] = {6, 2, 5, 5, 4, 5, 6, 3, 7, 6};
  int t;
  string x;
  cin >> t;

  for (int i = 0; i < t; i++) {
    cin >> x;
    int count = 0;
    for (int j = 0; j < x.size(); j++) {
      count += vec[x[j] - '0'];
    }
    cout << count << " leds" << endl;
  }

  return 0;
}
