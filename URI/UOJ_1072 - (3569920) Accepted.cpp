#include <bits/stdc++.h>
#define fast ios_base::sync_with_stdio(false);cin.tie(NULL)

using namespace std;

int main() {
  fast;
  int t, x, in = 0, out = 0;
  cin >> t;
  for (int i = 0; i < t; i++) {
      cin >> x;
      if (x >= 10 && x <= 20) in++;
      else out++;
  }

  cout << in << " in" << endl;
  cout << out << " out" << endl;

  return 0;
}
