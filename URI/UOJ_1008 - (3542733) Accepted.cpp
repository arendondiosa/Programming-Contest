#include <bits/stdc++.h>
#define fast ios_base::sync_with_stdio(false);cin.tie(NULL)

using namespace std;

int main() {
  fast;
  int a, b;
  float c;
  scanf("%d %d %f", &a, &b, &c);
  printf("NUMBER = %d\n", a);
  printf("SALARY = U$ %.2f\n",b * c);

  return 0;
}
