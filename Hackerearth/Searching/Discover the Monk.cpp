//Discover the Monk
//https://www.hackerearth.com/code-monk-searching/algorithm/discover-the-monk/

#include <bits/stdc++.h>
#define fast ios_base::sync_with_stdio(false);cin.tie(NULL)

using namespace std;

int main(){
    fast;
    int n, q, x;
    cin >> n >> q;
    vector<int> vec(n);
    for (int i = 0; i < n; i++) cin >> vec[i];
    sort(vec.begin(), vec.end());
    for (int i = 0; i < q; i++) {
        cin >> x;

        if (binary_search(vec.begin(), vec.end(), x)) cout << "YES" << endl;
        else cout << "NO" << endl;
    }

    return 0;
}
