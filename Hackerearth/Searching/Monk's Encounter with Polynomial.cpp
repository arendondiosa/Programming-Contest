//Monk's Encounter with Polynomial
//https://www.hackerearth.com/code-monk-searching/algorithm/monks-encounter-with-polynomial/

#include <bits/stdc++.h>
#define fast ios_base::sync_with_stdio(false);cin.tie(NULL)

using namespace std;

int main(){
  fast;
  int t, a, b, c, k;
  cin >> t;
  for (int i = 0; i < t; i++) {
    cin >> a >> b >> c >> k;
    for(int j = 0; j < 100000; j++) {
      if (((a * (j * j)) + (b * j) + c) >= k) {
        cout << j << endl;
        break;
      }
    }
  }

  return 0;
}
