//Chandu and his Girlfriend
//https://www.hackerearth.com/code-monk-sorting/algorithm/chandu-and-his-girlfriend/

#include <bits/stdc++.h>
#define fast ios_base::sync_with_stdio(false);cin.tie(NULL)

using namespace std;

int main(){
    fast;
    int t, n;
    cin >> t;

    for ( int i = 0; i < t; i++) {
        cin >> n;
        vector<int> vec(n);
        for (int j = 0; j < n; j++) {
            cin >> vec[j];
        }
        sort(vec.rbegin(), vec.rend());

        for (int j = 0; j < n - 1; j++) {
            cout << vec[j] << " ";
        }
        cout << vec[n-1] << endl;
    }

    return 0;
}
