//Chandu and his Girlfriend Returns
//https://www.hackerearth.com/code-monk-sorting/algorithm/chandu-and-his-girlfriend-returns/

#include <bits/stdc++.h>
#define fast ios_base::sync_with_stdio(false);cin.tie(NULL)

using namespace std;

int main(){
    fast;
    int t, n, m, x;
    cin >> t;

    for ( int i = 0; i < t; i++) {
        cin >> n >> m;
        vector<int> vec;
        for(int j = 0; j < (n + m); j++) {
            cin >> x;
            vec.push_back(x);
        }
        sort(vec.rbegin(), vec.rend());

        for (int j = 0; j < vec.size(); j++) cout << vec[j] << " ";
        cout << endl;
    }

    return 0;
}
