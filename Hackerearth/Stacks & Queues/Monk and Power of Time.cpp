//Monk and Power of Time
//https://www.hackerearth.com/code-monk-stacks-queues/algorithm/monk-and-power-of-time/

#include <bits/stdc++.h>
#define fast ios_base::sync_with_stdio(false);cin.tie(NULL)

using namespace std;

int main() {
  fast;
  queue<int> q;
  int n, aux;
  cin >> n;
  vector<int> vec(n);

  for (int i = 0; i < n; i++) {
    cin >> aux;
    q.push(aux);
  }

  for (int i = 0; i < n; i++) cin >> vec[i];

  int t_time = 0, exec = 0;

  while (!q.empty()) {
    int act = q.front();
    if (act == vec[exec]) {
      q.pop();
      t_time++;
      exec++;
    }
    else {
      q.pop();
      q.push(act);
      t_time++;
    }
  }

  cout << t_time << endl;

  return 0;
}
