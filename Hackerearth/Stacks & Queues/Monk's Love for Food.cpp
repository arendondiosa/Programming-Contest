//Monk's Love for Food 
//https://www.hackerearth.com/problem/algorithm/monks-love-for-food/description/

#include <bits/stdc++.h>
#define fast ios_base::sync_with_stdio(false);cin.tie(NULL)

using namespace std;

int main() {
  fast;
  int t, x, y;
  cin >> t;
  stack<int> stk;

  for (int i = 0; i < t; i++) {
    cin >> x;
    if (x == 2) {
      cin >> y;
      stk.push(y);
    }
    else {
      if (stk.empty()) cout << "No Food" << endl;
      else {
        cout << stk.top() << endl;
        stk.pop();
      }
    }
  }

  return 0;
}
