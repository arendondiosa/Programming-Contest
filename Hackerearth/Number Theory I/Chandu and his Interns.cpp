//Chandu and his Interns
//https://www.hackerearth.com/code-monk-number-theory-i/algorithm/chandu-and-his-interns/

#include <bits/stdc++.h>

using namespace std;

int main() {
  int n, x;
  cin >> n;

  for (int j = 0; j < n; j++) {
    int count = 0;
    cin >> x;
    for (int i=1; i*i<=x; i++) {
      if (x%i == 0) {
        count++;
        if (i * i < x) count++;
      }
    }
    if (count >= 4) cout << "YES" << endl;
    else cout << "NO" << endl;
  }

  return 0;
}
