//Chandu and Consecutive Letters
//https://www.hackerearth.com/code-monk-array-strings/algorithm/chandu-and-consecutive-letters/

#include <bits/stdc++.h>

using namespace std;

int main(){
    int t;
    string x;
    char y;
    cin >> t;

    for (int i = 0; i < t; i++) {
        cin >> x;
        y = x[0];
        cout << y;
        for (int j = 1; j < x.size(); j++) {
            if(x[j] == y) continue;
            else {
                y = x[j];
                cout << y;
            }
        }
        cout << endl;
    }

    return 0;
}
