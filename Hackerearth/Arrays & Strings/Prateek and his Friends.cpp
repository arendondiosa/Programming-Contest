//Prateek and his Friends
//https://www.hackerearth.com/code-monk-array-strings/algorithm/prateek-and-his-friends/description/

#include <bits/stdc++.h>
#define fast ios_base::sync_with_stdio(false);cin.tie(NULL)

using namespace std;

int main(){
    fast;
    int t, n, cost, x;
    bool flag;
    cin >> t;
    for (int i = 0; i < t; i++) {
        flag = false;
        cin >> n >> cost;
        vector<int> vec (n);
        cin >> vec[0];
        if (vec[0] == cost) flag = true;

        for (int j = 1; j < n; j++) {
            cin >> x;
            vec[j] = x + (vec[j - 1]);
            if (vec[j] == cost) flag = true;
        }

        if (!flag) {
            for (int j = 0; j < n; j++) {
                if(!flag) {
                    for (int k = j+1; k < n; k++) {
                        if ((vec[k] - vec[j]) > cost) break;
                        else if ((vec[k] - vec[j]) == cost) {
                            flag = true;
                            break;
                        }
                    }
                }
                else break;
            }
        }

        if (flag) cout << "YES" << endl;
        else cout << "NO" << endl;
    }

    return 0;
}
