//B. Dinner with Emma
//http://codeforces.com/contest/616/problem/B

#include <bits/stdc++.h>
#define fast ios_base::sync_with_stdio(false);cin.tie(NULL)

using namespace std;

int main(){
  fast;
  int n, m, x, min;
  cin >> n >> m;
  vector< vector<int> > vec(n, vector<int> (m));
  vector<int> mins (n);

  for (int i = 0; i < n; i++) {
    min = 1000000001;
    for (int j = 0; j < m; j++) {
      cin >> x;
      if (x < min) min = x;
      vec[i].push_back(x);
    }
    mins[i] = min;
  }
  int max = 0;
  for (int i = 0; i < n; i++) {
    if (mins[i] > max) max = mins[i];
  }

  cout << max << endl;

  return 0;
}
