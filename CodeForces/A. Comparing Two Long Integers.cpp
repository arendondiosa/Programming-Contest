//A. Comparing Two Long Integers
//http://codeforces.com/contest/616/problem/A

#include <bits/stdc++.h>
#define fast ios_base::sync_with_stdio(false);cin.tie(NULL)

using namespace std;

int main(){
  fast;
  unsigned long long x, y;
  cin >> x >> y;

  if (x > y) cout << ">" << endl;
  else if (x < y) cout << "<" <<endl;
  else cout << "=" << endl;

  return 0;
}
